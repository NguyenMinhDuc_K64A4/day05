<!DOCTYPE html> 

<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Login</title>
<body>

    <fieldset style='width: 500px; height: 600; border:#ADD8E6 solid'>
    <?php
    session_start();

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $check = true;
            if(empty(inputHandling($_POST["name"]))){
                echo "<div style='color: red;'>Hãy nhập tên</div>";
                $check=false;
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính</div>";
                $check=false;
            }
            if(empty(inputHandling($_POST["industryCode"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
                $check=false;
            }
            $birthOfDate = inputHandling($_POST["birthOfDate"]);
            if(empty($birthOfDate)){
                echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
                $check=false;
            }
            elseif (!validateDate($birthOfDate)) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
                $check=false;
            }
            if($check){
                $_SESSION = $_POST;
                $_SESSION["image"] = $_FILES["image"]["name"];
                $files = $_FILES["image"]["tmp_name"];
                header("location: submit.php");
                $path = "img/" . $_SESSION["image"];
                move_uploaded_file($files,$path);
            }
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }

        }
    ?>
    <form style='margin: 20px 50px 0 35px' action="submit" method="post" enctype = "multipart/form-data" >
        <table style = 'border-collapse:separate; border-spacing:15px 15px;'>
            <tr height = '40px'>
                <td style = 'background-color: #38e332; 
                vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Họ và tên</label>
                    <span style='color: red;'>*</span>
                </td>
                <td >
                    <input type='text' name = "name" style = 'line-height: 32px; border-color:#ADD8E6'>
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #38e332; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Giới tính</label>
                    <span style='color: red;'>*</span>
                </td>
                <td >
                    <?php
                        $genderArr=array("Nam","Nữ");
                        for($x = 0; $x < count($genderArr); $x++){
                            echo"
                                <label class='container'>
                                    <input type='radio' value=".$genderArr[$x]." name='gender'>"
                                    .$genderArr[$x]. 
                                "</label>";
                        }
                    ?>  
                </td>
            </tr>
            <tr height = '40px'>
                <td style = 'background-color: #38e332; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Phân Khoa</label>
                    <span style='color: red;'>*</span>
                </td>
                <td height = '40px'>
                    <select name='industryCode' style = 'border-color:#ADD8E6;height: 100%;width: 80%;'>
                        <?php
                            $industryCodeArr=array("EMPTY"=>"","MAT"=>"Khoa học máy tính","KDL"=>"Khoa học vật liệu");
                            foreach($industryCodeArr as $x=>$x_value){
                                echo"<option>".$x_value."</option>";
                            }
                        ?>
                    </select>
                </td>
            </tr>
            
            <tr height = '40px'>
                <td style = 'background-color: #38e332; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Ngày sinh</label>
                    <span style='color: red;'>*</span>
                </td>
                <td height = '40px'>
                    <input type='date' name="birthOfDate" data-date="" data-date-format="DD MMMM YYYY" style = 'line-height: 32px; border-color:#ADD8E6'>
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #38e332; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Địa chỉ</label>
                </td>
                <td height = '40px'>
                    <input type='text' name="address" style = 'line-height: 32px; border-color:#ADD8E6'> 
                </td>
            </tr>

            <tr height = '40px'>
                <td style = 'background-color: #38e332; vertical-align: top; text-align: left; padding: 5px 5px'>
                    <label style='color: white;'>Hình ảnh</label>
                </td>
                <td height = '40px'>
                <input type="file" name="image" id="picture"><br>
                </td>
            </tr>

        </table>
        <button style='background-color: #49be25; border-radius: 10px; 
        width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white;'>Đăng Kí</button>
    </form>

</fieldset>
</body>
</html>
